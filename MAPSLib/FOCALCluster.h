#ifndef FOCALCLUSTER_H
#define FOCALCLUSTER_H

#include "FOCALHit.h"
#include "LookUpTables.h"
#include "TClonesArray.h"

class FOCALCluster : public TObject
{
	public:
		//functions
		FOCALCluster(FOCALHit *Hit = NULL);
		FOCALCluster(const FOCALCluster &OtherCluster);
		~FOCALCluster();
		void AddHit(FOCALHit *NewHit);
		Long_t Chip();
		TObject *Clone(const char *newname = "");
		void GetCoordinates(Double_t *Array);
		FOCALHit *GetHit(Long_t Entry);
		Long_t GetHits() {return Hits->GetEntriesFast();}
		Long_t Layer();
		Bool_t IsNeighbouring(FOCALCluster *OtherCluster);
		Bool_t IsNeighbouring(FOCALHit *NewHit);
		void Merge(FOCALCluster *OtherCluster);
		void RemoveDuplicates();
		void RemoveHit(Long_t Entry);
		Double_t ClusterX();
		Double_t ClusterY();
		Double_t Z();
	private:
		//data members
		static LookUpTables *LUT;
		TClonesArray *Hits;
		//functions
		void CleanUp();
		Bool_t HitIsPresent(Long_t Entry);
		void Init();
		ClassDef(FOCALCluster, 1) //class title definition
};

#endif //FOCALCLUSTER_H
