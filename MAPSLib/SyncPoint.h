#ifndef SYNCPOINT_H
#define SYNCPOINT_H

#include "LookUpTables.h"

class SyncPoint : public TObject
{
	public:
		SyncPoint();
		SyncPoint(Long_t ONumber, Long_t MNumber);
		SyncPoint(const SyncPoint& OtherObject);
		~SyncPoint();
		TObject *Clone(const char *newname = "");
		Long_t Matching() {return MatchingBits;}
		Long_t Offset() {return BitOffset;}
	private:
		//data members
		static LookUpTables *LUT;
		static Long_t Counter;
		Long_t BitOffset, MatchingBits, Serial;
		//functions
		void Init();
		ClassDef(SyncPoint, 1) //class title definition
};

#endif /* SYNCPOINT_H */
