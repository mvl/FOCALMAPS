#ifndef CREATEDENSITIES_H
#define CREATEDENSITIES_H

#include "TObject.h"
#include "TObjArray.h"

class Analysis;
class FOCALFrame;
class FOCALTrack;
class FOCALHit;
class VirtexPedestal;
class LookUpTables;
class ChipPedestal;
class ChipResponse;
class DetectorResponse;

class TTree;
class TObjArray;
class TH2I;

class CreateDensities : public TObject
{

public:

	CreateDensities();
	~CreateDensities();
	Bool_t GoodRuns(long CurrentRun);
	Bool_t MoliereLayer( long Layer);
	Bool_t PosLayer(long Layer );
	void SetTriggerBits();
	void Init();
	void GetFactors();
	void CleanUp();
	
	void OpenShowerFile(long Run);
	Bool_t TriggerCollision(FOCALFrame *Frame);
	TH2I *Make2DFrame(long Layer);
	double *Parameter( FOCALFrame *Frame);
	double *ThisSeedPosition(FOCALFrame* Frame);
	double *ThisReSeedPosition(FOCALFrame* Frame);
	double *OneLayerSeedPosition(FOCALFrame* Frame, long LayerN);
	long HitsInCentral(FOCALFrame* Frame, double X, double Y);
	//double *Arraypixels( FOCALFrame* Frame, double X, double Y, long Layer, double Rangein, double Rangeout);
	void Arraypixels( FOCALFrame* Frame, double X, double Y, long Layer);		// fast than previous 
	long HitsForLayers( FOCALFrame* Frame, double X, double Y, double Range );
	double *creatCluster(FOCALFrame* Frame, double Px, double Py, double searchingR=0.05);
	Bool_t FrameSelection(FOCALFrame* Frame);
	TH2I *ShowerPosition(long Run);
	void ShowerPosition(long StartRun, long StopRun);
	void creatShowerClusters(FOCALFrame* Frame);
	void HitsInLayerRadius( FOCALFrame* Frame);
	void WriteTTree(long Run, long No=-1);
	void WriteTTreeRuns(long StartRun, long StopRun);
	void ReadDensity(long StartRun, long StopRun, long Q);
	void ReadAllDensity(long StartRun, long StopRun);
	TObjArray *Layerhits(FOCALFrame* Frame, double Px, double Py, long Layer);
	void hitsmapForLayer(long StartRun, long StopRun, long Layer);
	void patternParameters(long StartRun, long StopRun);
	void sensitivity(long StartRun, long StopRun);
	TObjArray *ReturnClusters;
	void GetSpecifiedHits(FOCALFrame* Frame, long L=-1);
	static const char *DensityFilePath, *fFactors;

private:
	static LookUpTables *LUT;
    Analysis *Analy ;
    FOCALFrame *CurrentFrame;
    FOCALTrack *CurrentTrack;
    ChipPedestal *Noise;
    DetectorResponse *DR;
    ChipResponse *CR;
    //LineData *LineD;
    FOCALHit *CurrentHit;
    VirtexPedestal *VP;
    TTree *CurrentTree;    
    TObjArray LayerZeroHits;
    TObjArray LayersHits;


    double MaxR; 
    long  totalbins;
    
    static const long  totalsteps, NLayers, NChips,NQuadrants,  AcceptHits;	
    static const double  stepradius1, stepradius2, binsize, SPSRuns[], decline[2];
    
    double Sensorfactors[96], Factors[24][4],  *SP, *Position,  *cluster, *SeedPosition, *ReSeedPosition, *RoughPosition, *OneLayerSP,  *parameter, *ReturnPtr, *AllLayerSP;// *decline;
    
ClassDef(CreateDensities, 1) //class title definition
};

#endif //CREATEDENSITIES








