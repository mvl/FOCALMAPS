#ifndef FOCALTRACK_H
#define FOCALTRACK_H

//ROOT classes
#include "TClonesArray.h"
#include "TPolyLine3D.h"
#include "TPolyMarker3D.h"
//own classes
#include "FOCALCluster.h"
#include "LookUpTables.h"
#include "TrackCandidate.h"

class FOCALTrack : public TObject
{
	public:
		//members
		Long_t Score, TrackNumber, Run, Trigger, Shower;
		//functions
		FOCALTrack(TrackCandidate *TC = NULL);
		FOCALTrack(const FOCALTrack& OtherTrack);
		~FOCALTrack();
		void AddCluster(Long_t HitAsSeed = -1);
		void AddHit(FOCALHit *NewHit);
		void AddHit(Long_t Chip, Long_t Line, Long_t Bit);
		Double_t *CalculateIntercepts();
		TObject *Clone(const char *newname = "");
		Int_t Compare(const TObject *Obj) const;
		void Draw(Option_t *option = "");
		const Long_t GetClusters() const {return Long_t(Clusters->GetEntriesFast());}
		FOCALCluster *GetCluster(Long_t Cluster);
		const Long_t GetHits() const {return Long_t(Hits->GetEntriesFast());}
		TObjArray *GetHitsInLayer(Long_t Layer);
		FOCALHit *GetHit(Long_t Hit);
		Long_t GetHitNumber(FOCALHit *Hit);
		TrackCandidate *GetSeed() {return Seed;}
		Long_t HitsInChip(Long_t Chip);
		Long_t *HitsPerChip();
		Long_t *HitsPerLayer();
		Long_t LargestClusterInChip(Long_t Chip);
		Long_t *LargestClustersInChips();
		Long_t LargestClusterInLayer(Long_t Layer);
		Long_t *LargestClustersInLayers();
		Long_t LayerHits(Long_t Layer);
		Double_t LayerX(Long_t Layer);
		Double_t LayerY(Long_t Layer);
		Double_t LayerZ(Long_t Layer);
		Bool_t IsSortable() const {return kTRUE;}
		void MakeClusters();
		TPolyLine3D *MakeLine();
		TPolyMarker3D *MakeMarkers();
		TPolyMarker3D *MakeExcludedPixels();
		void MergeClusters();
		void PrintStats();
		void RecalculateCumulants();
		void RemakeClusters();
		void RemoveHit(FOCALHit *Hit) {RemoveHit(GetHitNumber(Hit));}
		void RemoveHit(Long_t Hit);
		void RemoveCluster(Long_t Cluster);
		void SetSeed(TrackCandidate *NewSeed);
		Long_t Straighten();
	//private:
		//members
		Long_t NLayers, CumulantsPerLayer;
		Bool_t ClustersDone;
		static LookUpTables *LUT;
		static Double_t MaxDeviation, IncludeRange;
		TClonesArray *Hits, *Clusters, *RemovedHits;
		TrackCandidate *Seed;
		Double_t *LayerCumulants; //!
		//functions
		Double_t CalcAngle(Long_t Layer);
		void CleanUp();
		Bool_t ClusterIsPresent(Long_t Cluster);
		Bool_t HitIsPresent(Long_t Hit);
		void Init();
		void RemoveDuplicates();
		ClassDef(FOCALTrack, 1) //class title definition
};

#endif /* FOCALTRACK_H */
