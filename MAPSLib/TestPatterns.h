#ifndef TESTPATTERNS_H
#define TESTPATTERNS_H

#include "LookUpTables.h"
#include "TestPattern.h"

class TestPatterns : public TObject
{
	public:
		~TestPatterns();
		static TestPatterns *GetInstance();
		TestPattern *GetPattern(Long_t Channel, Long_t Pattern);
	private:
		//data members
		static TestPatterns *Instance;
		static LookUpTables *LUT;
		TObjArray *Patterns;
		//functions
		TestPatterns();  //N.B. !!! singleton constructor
		void Init();
		ClassDef(TestPatterns, 1) //class title definition
};

#endif /* TESTPATTERNS_H */
