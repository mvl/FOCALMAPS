#ifndef VIRTEXPEDESTAL_H
#define VIRTEXPEDESTAL_H

#include "ChipPedestal.h"
#include "LookUpTables.h"

class TH1I;
class TH2C;
class TH2D;
class TH3D;

class VirtexPedestal : public TObject
{
	public:
		VirtexPedestal();
		VirtexPedestal(Long_t V, DemuxedData *DMD = NULL, TriggerStream *TS = NULL);
		VirtexPedestal(const VirtexPedestal& OtherObject);
		~VirtexPedestal();
		TObject *Clone(const char *newname = "");
		TObjArray *DrawExcludedPixels(Long_t Chip = -1);
		TObjArray *DrawPedestal(Long_t Chip = -1);
		TObjArray *DrawProjection(Long_t Chip = -1);
		Double_t GetAverageNoise(Long_t Chip = -1);
		Long_t GetExcludedPixels(Long_t Chip = -1);
		ChipPedestal *GetPedestal(Long_t Chip);
		const ChipPedestal *GetPedestal(Long_t Chip) const;
		TH1I *GetNoiseSpectrum(Long_t Chip);
		Double_t GetNoiseThreshold(Long_t Chip);
		Double_t GetPixelNoise(Long_t Bit, Long_t Line, Long_t Chip);
		Double_t GetSigmaNoise(Long_t Chip = -1);
		Long_t GetVirtexNumber() {return Virtex;}
		Bool_t IncludePixel(Long_t Bit, Long_t Line, Long_t Chip);
		Long_t LoadData(DemuxedData *DMD, TriggerStream *TS);
		TH2C *MakeExcludeMap(Long_t Chip);
		TH2D *MakeHitMap(Long_t Chip);
		void PrintPedestalStatistics(Long_t Chip = -1);
		void SetAcceptThreshold(Double_t NewVal);
		void SetVirtexNumber(Long_t NewVal) {Virtex = NewVal;}
		
	private:
		//data members
		TObjArray *Pedestals;
		Long_t Virtex, Serial;
		static LookUpTables *LUT;
		static Long_t Counter;
		//functions
		void Init(Long_t V = 0);
		ClassDef(VirtexPedestal, 2) //class title definition
};



#endif /* VIRTEXPEDESTAL_H */
