#ifndef SYNCSTREAM_H
#define SYNCSTREAM_H

#include "LookUpTables.h"
#include "SyncPoint.h"

class TCanvas;

class SyncStream : public TObject
{
	public:
		SyncStream();
		SyncStream(const SyncStream& OtherObject);
		~SyncStream();
		void AddSync(SyncPoint *NewSync);
		void AddSync(Long_t Offset, Long_t Matching);
		Bool_t AmIPopulated() {return Populated;}
		TObject *Clone(const char *newname = "");
		void Filter(Long_t Threshold);
		Long_t GetEntries();
		SyncPoint *GetSync(Long_t Index);
		Long_t GetTP0Found() {return TP0FoundLocation;}
		Long_t Highest();
		Long_t Lowest();
		void RemoveSync(Long_t Index);
		void SetPopulated(Bool_t NewVal = kTRUE) {Populated = NewVal;}
		void SetTP0Found(Long_t NewVal) {TP0FoundLocation = NewVal;}
		TCanvas *ShowStream(TString Name = "Synchronization Stream");
	private:
		//data members
		static Long_t Counter;
		static LookUpTables *LUT;
		TObjArray *Data;
		Bool_t Populated, Sorted;
		Long_t TP0FoundLocation;
		Long_t Serial;
		//functions
		void Init();
		void Sort();
		ClassDef(SyncStream, 1) //class title definition
};

#endif /* SYNCSTREAM_H */
