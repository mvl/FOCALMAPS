#ifndef FOCALNewHit_H
#define FOCALNewHit_H

#include "LookUpTables.h"

class FOCALNewHit : public TObject
{
	public:
		//data members
		Long_t Chip, Line, Bit;
		Double_t HitX, HitY;
		//functions
		FOCALNewHit(Long_t C = -1, Long_t L = -1, Long_t B = -1);
		FOCALNewHit(Double_t HitX, Double_t HitY);
		FOCALNewHit(const FOCALNewHit &OtherHit);
		~FOCALNewHit();
        
		static Double_t CalcX(Long_t C, Long_t L, Long_t B);
		static Double_t CalcY(Long_t C, Long_t L, Long_t B);
		static Double_t CalcZ(Long_t C, Long_t L, Long_t B) {return LayerToZ(Layer(Group(C)));}
		TObject *Clone(const char *newname = " ");
		Double_t Distance(FOCALNewHit *OtherHit = NULL);
		Double_t Distance(Double_t x, Double_t y, Double_t z);
		Double_t Distance(Double_t x, Double_t y, Long_t L){ return Distance(x,y,LayerToZ(L)); }
		Double_t DistanceInSameLayer(FOCALNewHit *OtherHit = NULL);
		Double_t DistanceInSameLayer(Double_t x, Double_t y);
		void GetCoordinates(Double_t *Array);
		static Long_t Group(Double_t z);
		static Long_t Group(Long_t C) {return C/LUT->ChipsPerLayer;}
		static Long_t Layer(Double_t z);
		static Long_t Layer(Long_t G);
		Long_t Layer() {return Layer(Group(Chip));}
		static Double_t LayerToZ(Long_t L);
		static void InvertCoordinates(Double_t x, Double_t y, Double_t z, Long_t *Array);
		static void InvertCoordinates(Double_t x, Double_t y, Long_t L, Long_t *Array);
		static void InvertCoordinates2(Double_t x, Double_t y, Double_t z,TObjArray *ReturnPtr);    //new invert coordinate2.
		Bool_t IsNeighbouring(FOCALNewHit *OtherHit, Bool_t IncludeDiag = kTRUE);
		Bool_t IsValid();
		Bool_t operator!=(const FOCALNewHit &OtherHit) {return !(*this==OtherHit);}
		Bool_t operator==(const FOCALNewHit &OtherHit);
		static Long_t Quadrant(Double_t x, Double_t y);
		static Long_t Quadrant(Long_t C) {return C%LUT->ChipsPerLayer;}
		void SetValues(Long_t C, Long_t L, Long_t B) {Chip = C; Line = L; Bit = B;}
		Double_t X() { return TMath::Cos(deltaTheta[Chip])*CalcX(Chip,Line,Bit)+TMath::Sin(deltaTheta[Chip])*CalcY(Chip,Line,Bit)+deltaX[Chip];
		   // cout<<Chip<<" / "<<deltaX[Chip]<<" / "<< deltaY[Chip] <<" / "<< deltaTheta[Chip] << endl;
		}
		Double_t Y() {return TMath::Cos(deltaTheta[Chip])*CalcY(Chip,Line,Bit)-TMath::Sin(deltaTheta[Chip])*CalcX(Chip,Line,Bit)+deltaY[Chip];}
		Double_t Z() {return CalcZ(Chip,Line,Bit);}
		
		static Double_t Z0() {return LayerToZ(24);}		
	private:
		//data members
		static bool initialized;
	    static bool Init(TString FileName = "Alignment_2014Pion.txt");
		static LookUpTables *LUT;
	    static Double_t *deltaX;
	    static Double_t *deltaY;
	    static Double_t *deltaTheta;

		ClassDef(FOCALNewHit, 1) //class title definition
};

#endif //FOCALNewHit_H
