#ifndef RAWDATA_H
#define RAWDATA_H

#include "LookUpTables.h"

class FOCALBuffer;
class DemuxedData;
class SyncStream;
class TestPatterns;
class TH1I;
class TriggerStream;
class VirtexHeader;

class RawData : public TObject
{
	public:
		RawData();
		RawData(char *Input, Long_t Bytes, Long_t Number = 0);
		RawData(FOCALBuffer *FB, Long_t Number = 0);
		RawData(Long_t Run, Long_t Trigger, Long_t Virtex);
		RawData(const RawData& OtherObject);
		~RawData();
		Bool_t ChannelBit(Long_t Channel, Long_t Bit);
		DemuxedData *DemuxData();
		SyncStream *GetChannelSyncs(Long_t Channel);
		Long_t GetNumber() {return VirtexNumber;}
		Long_t GetSize();
		Bool_t Initialized() {return DataRead;}
		void LoadHeader(VirtexHeader *NewHeader);
		TH1I *MakeSyncPlot(Long_t Trigger = -1);
		void SetBitNumber(Long_t Number, Bool_t Val = kTRUE);
		Long_t SyncData();
		Bool_t Synced() {return (Bool_t)Syncs;}
		Bool_t TestBitNumber(Long_t Number);
	private:
		//data members
		static Long_t Counter;
		FOCALBuffer *Data;
		Bool_t DataRead;
		Long_t VirtexNumber, Serial;
		TObjArray *Syncs;
		static TestPatterns *TP;
		static LookUpTables *LUT;
		VirtexHeader *VH;
		//functions
		void DemuxChannel(Long_t Channel, DemuxedData *DMD);
		void FindCheckerBoardPatterns();
		void FindTestPatterns();
		void Init();
		void IndicateSyncedChannels(DemuxedData *DMD);
		void ReadData(FOCALBuffer *CI);
		void ReadData(char *Input, Long_t Bytes);
		ClassDef(RawData, 1) //class title definition
};

//define maxsearchradius for syncs and maybe some other parameters, maybe in LookUpTables?

#endif /* RAWDATA_H */
