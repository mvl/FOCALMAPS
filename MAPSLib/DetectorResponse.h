#ifndef DETECTORRESPONSE_H
#define DETECTORRESPONSE_H

#include "ChipResponse.h"

class DetectorResponse : public TObject
{
	public:
		DetectorResponse();
		~DetectorResponse();
		void AddTrack(FOCALTrack *NewTrack, FOCALFrame *NewFrame);
		ChipResponse *GetChipResponse(Long_t Chip);
		static void SetPedestals(VirtexPedestal **NewPointer);
	private:
		//members
		static LookUpTables *LUT;
		Long_t NChips;
		TClonesArray *ChipResponses;
		static VirtexPedestal **VP;
		//functions
		void CleanUp();
		void Init();
		ClassDef(DetectorResponse,1)
};

#endif //DETECTORRESPONSE_H
