#ifndef TRACKCANDIDATE_H
#define TRACKCANDIDATE_H

#include "FOCALHit.h"
#include "LookUpTables.h"

class TrackCandidate : public TObject
{
	public:
		//members
		FOCALHit *FirstHit, *SecondHit;
		Long_t N1, N2;
		Double_t Score, Xin, Yin, Xout, Yout;
		//functions
		TrackCandidate();
		TrackCandidate(FOCALHit *Hit1, FOCALHit *Hit2, Long_t Number1, Long_t Number2, Double_t S = -1.0);
		TrackCandidate(const TrackCandidate &OtherCandidate);
		~TrackCandidate();
		TObject *Clone(const char *newname = "");
		Int_t Compare(const TObject *Obj) const;
		Double_t DeltaZ();
		Double_t Distance(FOCALHit *Hit);
		Double_t Distance(TrackCandidate *OtherCandidate);
		void GetCoordinates(Double_t *Array);
		Bool_t IsSortable() const {return kTRUE;}
		static Double_t Z0() {return FOCALHit::Z0();}
	private:
		//data members
		static LookUpTables *LUT;
		//functions
		void Init();
		Double_t Xi();
		Double_t Xo();
		Double_t Yi();
		Double_t Yo();
		ClassDef(TrackCandidate,1)
};

#endif /*TRACKCANDIDATE_H*/
