#ifndef LOOKUPTABLES_H
#define LOOKUPTABLES_H

//ROOT classes
#include "Riostream.h"
#include "TBits.h"
#include "TFile.h"
#include "THistPainter.h"
#include "TKey.h"
#include "TMath.h"
#include "TObject.h"
#include "TObjArray.h"
#include "TString.h"
#include "TStyle.h"
  
class LookUpTables : public TObject
{
	public:
		~LookUpTables();
		static LookUpTables *GetInstance();
		//base constants
		static const Long_t L1MUXES, L2MUXES, L3MUXES, ChipsPerSpartan, SpartansPerVirtex, NVirtex, LinesPerFrame, TicksPerLine, PixelDataSize;
		static const Long_t ChipsPerLayer, NLayers, Synchronization_TPMaxSearchRadius, Synchronization_TP0MaxFrameSearch, Demuxing_RuntFrameThreshold, MaxPlotsPerCanvas;
		static const Double_t PixelSizeBit, PixelSizeLine, Synchronization_AcceptThreshold;
		static Long_t LoggingLevel;
		static const char *FOCALDAQServer_LogFileName, *FOCALDAQServer_DataPath, *Pedestal_Filename_Format, *NewMask_Filename_Format, *PixelData_Filename_Format, *TriggerData_Filename_Format, *DemuxedData_Filename_Format, *ProcessedData_Filename_Format,*MaskDataPath,*Overlap_Filename_Format,  *QC_Filename_Format;
		static const char *TestPattern_SourceFile, *TriggerHeader_Filename_Format,
		*PixelHeader_Filename_Format, *SyncData_Name_Prefix, *Shower_Histo_Format,  *DetectorResponse_Track;
		//derived constants
		Long_t BitsPerChannel() {return (L2MUXES*L3MUXES);}
		Long_t BitsPerLine() {return (L1MUXES*BitsPerChannel());}
		Long_t ChannelBit(Long_t Channel, Long_t Bit);
		Long_t NChannels() {return (NChips()*L1MUXES);}
		Long_t NChips() {return (ChipsPerSpartan*SpartansPerVirtex);}
		Long_t RawBitsPerLine() {return (BitsPerLine()*NChips());}
		//pointers to translation matrices
		Bool_t BitInLine(Long_t Bit);
		Bool_t BitOnChip(Long_t Bit);
		Bool_t ChannelInRange(Long_t Channel);
		Bool_t ChipInRange(Long_t Chip);
		Long_t *GetChannelsToMimosa() {return ChannelsToMimosa;}
		Long_t *GetChannelsToRAM() {return ChannelsToRAM;}
		Long_t *GetMimosaToChannels() {return MimosaToChannels;}
		Long_t *GetMimosaToRAM() {return MimosaToRAM;}
		Long_t *GetRAMToChannels() {return RAMToChannels;}
		Long_t *GetRAMToMimosa() {return RAMToMimosa;}
		Bool_t LineOnChip(Long_t Line);
		//other functions
		TString Directory(Long_t Run);
		TFile *OpenDMDFile(Long_t Run, Long_t Trigger, Long_t Virtex, const char *Options = "READ");
		TFile *OpenPedestalFile(Long_t Run, Long_t Virtex, const char *Options = "READ");
		TFile *OpenProcessedFile(Long_t Run, const char *Options = "READ");
		TFile *OpenRawFile(Long_t Run, Long_t Trigger, Long_t Virtex, const char *Options = "READ");
		TFile *OpenTriggerFile(Long_t Run, Long_t Trigger, Long_t Virtex, const char *Options = "READ");
		void SetStyle();
	private:
		//data members
		Long_t *RAMToChannels, *ChannelsToRAM, *RAMToMimosa, *MimosaToRAM, *ChannelsToMimosa, *MimosaToChannels;
		static LookUpTables *Instance;
		//functions
		LookUpTables();  //N.B. !!! singleton constructor
		void Init();
		ClassDef(LookUpTables, 1) //class title definition
};


#endif /* LOOKUPTABLES_H */
