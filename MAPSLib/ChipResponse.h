#ifndef CHIPRESPONSE_H
#define CHIPRESPONSE_H

#include "TrackSignal.h"

class ChipResponse : public TObject
{
	public:
		ChipResponse(Long_t Number = -1);
		~ChipResponse();
		void AddTrack(FOCALTrack *NewTrack, FOCALFrame *NewFrame);
		Long_t GetNumber() {return ChipNumber;}
		TrackSignal *GetSignal(Long_t Number);
		Long_t GetSignals() {return TrackSignals->GetEntriesFast();}
		static void SetPedestals(VirtexPedestal **NewPointer);
	private:
		//members
		static LookUpTables *LUT;
		Long_t ChipNumber;
		TClonesArray *TrackSignals;
		static VirtexPedestal **VP;
		//functions
		void CleanUp();
		void Init();
		ClassDef(ChipResponse,1)
};

#endif //CHIPRESPONSE_H
