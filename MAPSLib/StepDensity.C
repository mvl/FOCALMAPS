#include "StepDensity.h"

ClassImp(StepDensity)
//_______________________________________________
StepDensity::StepDensity()
{
Init();

} //default constructor

StepDensity::StepDensity(const StepDensity &OtherStep) : TObject()
{
Radius = OtherStep.Radius;
nrhits = OtherStep.nrhits;
workingpixels = OtherStep.workingpixels;
allpixels = OtherStep.allpixels;
noise = OtherStep.noise;
overlappixels = OtherStep.overlappixels;
overlapwokp = OtherStep.overlapwokp;
clusters = OtherStep.clusters; 
} //copy constructor

void StepDensity::SetStepDensity(double radius,long value0, long value1, long value2, double value3, long value4, long value5)
{ 
//printf("check data transfered here%.3f %ld %ld %ld %.3f %ld %ld \n", radius, value0,value1,value2,value3,value4, value5);
Radius = radius;
nrhits = value0;
workingpixels = value1;
allpixels = value2;
noise = value3;
overlappixels=value4;
overlapwokp=value5;

}
StepDensity::~StepDensity()
{
/*
Radius = 0;
nrhits = 0;
workingpixels = 0;
allpixels = 0;
noise = 0;
overlappixels=0;
overlapwokp=0;
clusters=NULL;
*/
}//destructor
//_______________________________________________
void StepDensity::Init()
{
Radius = 0;
nrhits = 0;
workingpixels = 0;
allpixels = 0;
noise = 0;
overlappixels=0;
overlapwokp=0;
clusters=NULL;
}


void StepDensity::StepClusterDensity(long clustersize )
{
	//clusters[clustersize]++;
}





