#ifndef PIXELSARRAY_H
#define PIXELSARRAY_H

#include "LayerDensity.h"
#include "TClonesArray.h"

class PixelsArray : public TObject
{

public:
PixelsArray();//default constructor
virtual ~PixelsArray();
void Init();

void SetRunnr(long);
long GetRunnr() { return Runnr; }

void SetShowerNumber(long);
long GetShowerNumber() { return ShowerNumber;}

void SetTriggerCounter(long);
long GetTriggerCounter() { return TriggerCounter; }

void SetShowerPosition(double, double);
double GetShowerPositionX() { return ShowerPositionX; }
double GetShowerPositionY() { return ShowerPositionY; }

void SetSpillnr(long);
long GetSpillnr(){ return Spillnr;}


LayerDensity *CreateLayerDensity();
void AddDensityLayer(LayerDensity *NewLayer);

LayerDensity *GetLayerDensity(long Layer);

private:

//TClonesArray* fPixelsArray;
long  Runnr, Spillnr,ShowerNumber, TriggerCounter;
double ShowerPositionX ,ShowerPositionY;
TClonesArray *Layers;
ClassDef(PixelsArray,1);

};

#endif



