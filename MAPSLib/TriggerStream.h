#ifndef TRIGGERSTREAM_H
#define TRIGGERSTREAM_H

#include "LookUpTables.h"
#include "FOCALTrigger.h"

class FOCALBuffer;
class TH1I;
class TH2I;
class TProfile;

class TriggerStream : public TObject
{
	public:
		TriggerStream();
		TriggerStream(char *Input, Long_t Size, Long_t Number = 0);
		TriggerStream(FOCALBuffer *FB, Long_t Number = 0);
		TriggerStream(Long_t Run, Long_t Trigger, Long_t Virtex);
		TriggerStream(const TriggerStream& OtherStream);
		~TriggerStream();
		void AddTrigger(FOCALTrigger *NewTrigger);
		TObject *Clone(const char *newname = "");
		TProfile *CompareVirtexCounters(TriggerStream* OtherStream);
		void Filter(Long_t *BitPattern = NULL);
		FOCALTrigger *FrameSyncAfter(Long_t Index);
		FOCALTrigger *FrameSyncBefore(Long_t Index);
		TObjArray *GetData() {return Data;}
		Long_t GetEntries();
		Long_t GetFirstSyncOffset();
		FOCALTrigger *GetTrigger(Long_t Index);
		FOCALTrigger *GetFilteredTrigger(Long_t Index);
		Long_t GetTriggerFrame(Long_t Index);
		Long_t GetTriggerLine(Long_t Index);
		Long_t GetValidTriggers();
		Bool_t IncludedTrigger(Long_t Index);
		Bool_t LineTriggered(Long_t Line, Long_t Frame = 0);
		TObjArray *MakeStats();
		Long_t *MatchTriggers(TriggerStream *OtherStream);
		void RemoveCollisions(Long_t MinDistance = 0);
		void RemoveTrigger(Long_t Index);
		TH2I *ShowStream();
		TH2I *ShowTriggerBits();
		TH2I *ShowOccupancy();
		TH1I *ShowTriggerCounter();
		TH1I *ShowOverFlow();
		TH1I *ShowCounts();
		Bool_t TriggerCollision(Long_t Index, Long_t Margin = LookUpTables::LinesPerFrame);
		void TriggerShiftAndDrift(TriggerStream *OtherStream, Double_t *&DriftMatrix, Long_t *&Matches);
	private:
		//data members
		static LookUpTables *LUT;
		static Long_t Counter;
		static const Int_t NBins = 10000;
		TBits *FilteredTriggers, *LinesWithTriggers;
		Long_t *CurrentFilter;
		Long_t FirstSyncOffset, VirtexNumber, Serial;
		TObjArray *Data;
		Bool_t Sorted, Compressed, FirstSyncFound, TriggeredLinesFound, Filtered;
		//functions
		void Compress();
		void FindFirstSyncOffset();
		void Init();
		void MakeLines();
		void SetFilter(Long_t *BitPattern);
		void Sort();
		ClassDef(TriggerStream, 1) //class title definition
};

#endif /* TRIGGERSTREAM_H */
