#ifndef TRIGGERHITS_H
#define TRIGGERHITS_H

#include "FOCALFrame.h"

class DemuxedData;
class VirtexPedestal;

class TriggerHits : public TObject
{
	public:
		TriggerHits();
		TriggerHits(Long_t V, Long_t R, Long_t T, DemuxedData *DMD = NULL, VirtexPedestal *VP = NULL);
		TriggerHits(const TriggerHits& OtherObject);
		~TriggerHits();
		void AnalyzeTriggers(DemuxedData *DMD, VirtexPedestal *VP);
		TObject *Clone(const char *newname = "");
		TObjArray *GetAnalyzedTriggers() {return AnalyzedTriggers;}
		Long_t GetEntries() {return Entries;}
		FOCALFrame *GetEntry(Long_t Entry);
		Long_t GetRunNumber() {return Run;}
		Long_t GetTriggerNumber() {return Trigger;}
		Long_t GetVirtexNumber() {return Virtex;}
		FOCALFrame *MakeFrame(DemuxedData *DMD, VirtexPedestal *VP, Long_t Entry);
		void SetRunNumber(Long_t NewVal) {Run = NewVal;}
		void SetTriggerNumber(Long_t NewVal) {Trigger = NewVal;}
		void SetVirtexNumber(Long_t NewVal) {Virtex = NewVal;}
	private:
		//data members
		static Long_t Counter;
		Long_t Virtex, Run, Trigger, Entries, Serial;
		TObjArray *AnalyzedTriggers;
		static LookUpTables *LUT;
		//functions
		void Init(Long_t V = -1, Long_t R = -1, Long_t T = -1);
		ClassDef(TriggerHits, 1) //class title definition
};

#endif /* TRIGGERHITS_H */
