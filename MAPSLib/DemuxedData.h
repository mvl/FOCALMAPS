#ifndef DEMUXEDDATA_H
#define DEMUXEDDATA_H

#include "LineData.h"
#include "LookUpTables.h"
#include "TriggerStream.h"

class DemuxedData : public TObject
{
	public:
		DemuxedData();
		DemuxedData(Long_t Number, TriggerStream *TS = NULL);
		DemuxedData(char *FileName);
		DemuxedData(Long_t Run, Long_t Trigger, Long_t Virtex);
		DemuxedData(const DemuxedData& OtherObject);
		~DemuxedData();
		void AddLine(LineData *NewLine);
		TObject *Clone(const char *newname = "");
		TObjArray *GetData() {return Data;}
		Long_t GetEntries();
		LineData *GetEntry(Long_t Entry);
		LineData *GetLine(Long_t Line, Long_t Frame = 0);
		FOCALTrigger *GetTrigger(Long_t Index);
		TriggerStream *GetTriggers() {return Triggers;}
		void LoadTriggers(TriggerStream *TS);
		void RemoveLine(Long_t Line, Long_t Frame);
		Int_t Write(const char* name = 0, Int_t option = 0, Int_t bufsize = 0);
	private:
		//data members
		static Long_t Counter;
		TriggerStream *Triggers;
		Long_t VirtexNumber, Serial;
		TObjArray *Data;
		static LookUpTables *LUT;
		static const Int_t InitialCapacity = 1000000;
		//functions
		void Init(Long_t Number = 0);
		Int_t ReadFile(char *FileName);
		ClassDef(DemuxedData, 1) //class title definition
};

#endif /* DEMUXEDDATA_H */
