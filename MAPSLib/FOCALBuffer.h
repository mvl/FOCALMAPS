#ifndef FOCALBUFFER_H
#define FOCALBUFFER_H

#include "LookUpTables.h"

class FOCALBuffer : public TObject
{
	public:
		//data members
		//functions
		FOCALBuffer();
		FOCALBuffer(Long_t NewVal, char *FileName = NULL);
		FOCALBuffer(const FOCALBuffer& OtherObject);
		~FOCALBuffer();
		TObject *Clone(const char *newname = "");
		void ConvertToPages();
		Long_t CountBits();
		char *CurrentPosition() {return (Array + BytesReceived);}
		void Finish() {Done = kTRUE;}
		Bool_t Finished() {return Done;}
		Long_t GetBytesReceived() {return BytesReceived;}
		TObjArray *GetArray() {return Data;}
		char *GetData() {return Array;}
		Long_t GetPages() {return NPages;}
		Long_t GetSize() {return Size;}
		void IncreaseBytesReceived(Long_t Input) {BytesReceived += Input;}
		static const char *Port(Long_t Type = -1);
		void SetBitNumber(Long_t Number, Bool_t Val = kTRUE);
		void SetData(char *Input, Long_t Bytes);
		Long_t SetSize(Long_t NewVal);
		Bool_t TestBitNumber(Long_t Number);
		Int_t Write(const char *name = 0, Int_t option = 0, Int_t bufsize = 0);
	private:
		//data members
		static const Long_t PageSize = 1048576;
		static Long_t Counter;
		static LookUpTables *LUT;
		char *Array;
		TObjArray *Data;
		Bool_t Done;
		Long_t Size, BytesReceived, ConnectionType, Serial, NPages;
		//functions
		Bool_t BitInRange(Long_t Number);
		void Init();
		Int_t ReadFile(char *FileName);
		ClassDef(FOCALBuffer, 1) //class title definition
};

#endif //FOCALBUFFER_H
