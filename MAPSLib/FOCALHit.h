#ifndef FOCALHIT_H
#define FOCALHIT_H

#include "LookUpTables.h"

class TH3D;

class FOCALHit : public TObject
{
	public:
		//data members
		Long_t Chip, Line, Bit;
                //TH3D* Overlap;
		//functions
		FOCALHit(Long_t C = -1, Long_t L = -1, Long_t B = -1);
		FOCALHit(const FOCALHit &OtherHit);
		~FOCALHit();
        
		static Double_t CalcX(Long_t C, Long_t L, Long_t B);
		static Double_t CalcY(Long_t C, Long_t L, Long_t B);
		static Double_t CalcZ(Long_t C, Long_t L, Long_t B) {return LayerToZ(Layer(Group(C)));}
		TObject *Clone(const char *newname = " ");
		Double_t Distance(FOCALHit *OtherHit = NULL);
		Double_t Distance(Double_t x, Double_t y, Double_t z);
		Double_t Distance(Double_t x, Double_t y, Long_t L){ return Distance(x,y,LayerToZ(L)); }
		Double_t DistanceInSameLayer(FOCALHit *OtherHit = NULL);
		Double_t DistanceInSameLayer(Double_t x, Double_t y);
		void GetCoordinates(Double_t *Array);
		static Long_t Group(Double_t z);
		static Long_t Group(Long_t C) {return C/LUT->ChipsPerLayer;}
		static Long_t Layer(Double_t z);
		static Long_t Layer(Long_t G);
		Long_t Layer() {return Layer(Group(Chip));}
		static Double_t LayerToZ(Long_t L);
		static void InvertCoordinates(Double_t x, Double_t y, Double_t z, Long_t *Array);
		static void InvertCoordinates(Double_t x, Double_t y, Long_t L, Long_t *Array);
		static void InvertCoordinates2(Double_t x, Double_t y, Double_t z,TObjArray *ReturnPtr);    //new invert coordinate2.
		static void InvertCoordinates2(FOCALHit *OtherHit,TObjArray *ReturnPtr) { InvertCoordinates2(OtherHit->X(), OtherHit->Y(), OtherHit->Z(),ReturnPtr); }
		//Bool_t IsOverlapPixel(Long_t C, Long_t L, Long_t B);		
		Bool_t IsNeighbouring(FOCALHit *OtherHit, Bool_t IncludeDiag = kTRUE);
		Bool_t IsValid();
		Bool_t operator!=(const FOCALHit &OtherHit) {return !(*this==OtherHit);}
		Bool_t operator==(const FOCALHit &OtherHit);
		static Long_t Quadrant(Double_t x, Double_t y);
		static Long_t Quadrant(Long_t C) {return C%LUT->ChipsPerLayer;}
		void SetValues(Long_t C, Long_t L, Long_t B) {Chip = C; Line = L; Bit = B;}
		Double_t X() { return TMath::Cos(deltaTheta[Chip])*CalcX(Chip,Line,Bit)+TMath::Sin(deltaTheta[Chip])*CalcY(Chip,Line,Bit)+deltaX[Chip];
		   // cout<<Chip<<" / "<<deltaX[Chip]<<" / "<< deltaY[Chip] <<" / "<< deltaTheta[Chip] << endl;
		}
		Double_t Y() {return TMath::Cos(deltaTheta[Chip])*CalcY(Chip,Line,Bit)-TMath::Sin(deltaTheta[Chip])*CalcX(Chip,Line,Bit)+deltaY[Chip];}
		Double_t Z() {return CalcZ(Chip,Line,Bit);}
		
		static Double_t Z0() {return LayerToZ(23);}		
	private:
		//data members
		static bool initialized;
	    static bool Init(TString FileName = "Alignment_Muon_2016.txt");
		static LookUpTables *LUT;
	    static Double_t *deltaX;
	    static Double_t *deltaY;
	    static Double_t *deltaTheta;

		ClassDef(FOCALHit, 1) //class title definition
};

#endif //FOCALHIT_H
