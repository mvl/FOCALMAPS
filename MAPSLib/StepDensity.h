#ifndef StepDensity_H
#define StepDensity_H

#include "TClonesArray.h"
class StepDensity : public TObject
{

public:
StepDensity();
StepDensity(const StepDensity &OtherStep);
~StepDensity();
void SetStepDensity(double radius,long value0, long value1, long value2, double value3,long value4, long value5);

void Init();
void StepClusterDensity(long );


long Getnrhits(){ return nrhits; }
long Getworkingpixels(){ return workingpixels; }
long Getallpixels(){ return allpixels; }
long Getoverlappixels(){ return overlappixels; }
long Getoverlapwokp(){ return overlapwokp; }
double GetRadius(){ return Radius; }
double Getnoise(){ return noise; }
long GetClusters(long clustersize) { return clusters[clustersize]; }

private:

double Radius, noise;
long nrhits, workingpixels, allpixels, overlappixels, overlapwokp;
long *clusters;

ClassDef(StepDensity,1);

};

#endif



