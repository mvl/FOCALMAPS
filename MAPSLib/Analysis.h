#ifndef ANALYSIS_H
#define ANALYSIS_H

//ROOT classes
class TCanvas;
class TH1;
class TH2I;
class TH3D;
class TTree;
//own classes
class FOCALBuffer;
class DemuxedData;
class FOCALFrame;
class FOCALTrack;
#include "LookUpTables.h"
class RawData;
class TriggerStream;
class TriggerHits;
class VirtexHeader;
class VirtexPedestal;
#include "TDatime.h"

class Analysis
{
	public:
		//data members
		
		static const char *ShowerTreeName, *ShowerBranchName, *TrackTreeName, *TrackBranchName, *TrackFileName;
		//functions
		Analysis();
		~Analysis();
		TH1 *CalcNoiseHits();
		void DemuxRun();
		void DemuxRuns(Long_t StartRun, Long_t StopRun);
		void DemuxTrigger();
		TObjArray *ExamineTrigger(Long_t Virtex, Long_t RunNumber, Long_t TriggerNumber);
		TCanvas *ExamineTriggersForRun(Long_t RunNumber, Long_t Virtex = 0, Long_t FirstTrigger = 0, Long_t LastTrigger = -1);
		void ExamineHeaders(Long_t RunNumber, Long_t Virtex = 0);
		void FindTracks(TObjArray *Buffer);
		void FindTracksThisRun();
		void FindTracksForRuns(Long_t StartRun, Long_t StopRun);
		FOCALFrame *GetFrame(Long_t R, Long_t S);
		FOCALFrame *GetNextFrame();
		FOCALFrame *GetNextFilteredFrame();
		FOCALTrack *GetNextTrack();
		Long_t GetRun() {return Run;}
		Long_t GetShower() {return CurrentShowerNumber-1;}
		Long_t GetTrack() {return CurrentTrackNumber-1;}
		TDatime *GetTime();
		Long_t GetTrigger() {return Trigger;}
		void IncreaseRun();
		void IncreaseTrigger();
		TObjArray *LookAroundTrigger(Long_t Virtex, Long_t Chip, Long_t NewRun, Long_t NewTrigger = 0);
		void MakePedestalForRun(Long_t Virtex, Long_t RunNumber, Long_t StartTrigger = 0, Long_t StopTrigger = -1);
		TH2I *MakeSyncsForRun(Long_t Virtex);
		void PrintFilter();
		void ProcessRun();
		void ProcessRuns(Long_t StartRun, Long_t StopRun);
		void ProcessTrigger(TObjArray *Buffer);
		void SetFilterBit(Long_t Bit, Long_t Status = -1);
		void SetRun(Long_t NewVal);
		void SetTrigger(Long_t NewVal = 0);

	//private:
		//members
		VirtexPedestal **VP;
		TriggerStream **TriggerData;
		DemuxedData **DMD;
		static LookUpTables *LUT;
		static const Long_t Analysis_NoiseFrameSamplesPerTrigger, MaxErrors, MaxBufferSize;
		Long_t NVirtex, Run, Trigger, TriggerBits, SequentialErrors, CurrentShowerNumber, CurrentTrackNumber;
		Long_t *Filter;
		RawData **RD;
		VirtexHeader **Headers;
		TFile *ProcessedData, *TrackData;
		TTree *CurrentTree, *TrackTree;
		FOCALFrame *CurrentShower;
		FOCALTrack *CurrentTrack;
                static TH3D* NewMask;
                TH3D* Overlap;
		//functions
		void CleanUp();
		TObjArray *ComputeEmptyFrames();
		Bool_t ContinueAnalysis() {return (SequentialErrors<MaxErrors);}
		TString CurrentDirectory();
		Bool_t DemuxedDataLoaded();
		Bool_t HeadersLoaded();
		void IncreaseErrors() {SequentialErrors++; printf("increased error count to %ld.\n",SequentialErrors);}
		void Init();
		void LoadDemuxedData();
		void LoadHeaders();
		void LoadPedestals();
		void LoadProcessedData();
		void LoadRawData();
		void LoadMask();
		void LoadOverlap();
		void LoadTracks();
		void LoadTriggerData();
		Bool_t MatchFilter(FOCALFrame *Shower);
		Bool_t PedestalsLoaded();
		Bool_t ProcessedDataLoaded();
		Bool_t RawDataLoaded();
		void ResetErrors() {SequentialErrors = 0;}
		Bool_t TracksLoaded();
		Bool_t TriggersLoaded();
		void UnloadDemuxedData();
		void UnloadHeaders();
		void UnloadPedestals();
		void UnloadProcessedData();
		void UnloadRawData();
		void UnloadMask();
		void UnloadOverlap();
		void UnloadTracks();
		void UnloadTriggerData();
		Bool_t VirtexNumberInRange(Long_t Virtex);
};
 
#endif /* ANALYSIS_H */
