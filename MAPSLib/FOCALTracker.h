#ifndef FOCALTRACKER_H
#define FOCALTRACKER_H

//ROOT classes
class TCanvas;
class THnSparse;
//own classes
class FOCALFrame;
class FOCALHit;
class FOCALTrack;
#include "LookUpTables.h"
#include "TClonesArray.h"
class TrackCandidate;

class FOCALTracker
{
	public:
		FOCALTracker();
		FOCALTracker(FOCALFrame *NewFrame);
		~FOCALTracker();
		void AssessCandidates();
		void FindTracks();
		TrackCandidate *GetCandidate(Long_t Candidate);
		Long_t GetCandidates();
		FOCALHit *GetHit(Long_t Hit);
		Long_t GetHits();
		FOCALTrack *GetTrack(Long_t Track);
		Long_t GetTracks();
		void LoadData(FOCALFrame *NewFrame);
		TObjArray *MakeProjections(Int_t Dimension = 2);
		void MakeSeeds();
		void MakeTrack(Long_t Track);
		void ScoreCandidates();
		void Seed();
		TCanvas *ShowFrame(Option_t *option = "");
		TCanvas *ShowSeeds1D();
		TCanvas *ShowSeeds2D();
		void SortCandidates();
		void SortTracks();
		void StraightenTracks();
		//private:
		//members
		static LookUpTables *LUT;
		static Int_t GlobalSerial;
		Long_t TrackThreshold, MinLayersWithHits;
		Double_t SizeX, SizeY;
		Int_t LocalSerial, BinSize, NumberOfDimensions;
		FOCALFrame *CurrentFrame;
		TClonesArray *Candidates, *Tracks, *AvailableHits;
		THnSparse *Seeds;
		//functions
		void CalculateCandidate(Long_t N1, Long_t N2);
		void CleanUp();
		Double_t DistanceToCandidate(Long_t Hit, Long_t Candidate);
		Bool_t IncludeCombo(Long_t N1, Long_t N2);
		void Init();
};

#endif /* FOCALTRACKER_H */
