#ifndef LINEDATA_H
#define LINEDATA_H

#include "LookUpTables.h"

class LineData : public TObject
{
	public:
		LineData();
		LineData(Long_t Line, Long_t Frame);
		LineData(char *Input, Long_t Line = 0, Long_t Frame = 0);
		LineData(const LineData& OtherObject);
		~LineData();
		Bool_t ChannelWorking(Long_t Channel = 0);
		TObject *Clone(const char *newname = "");
		Long_t GetChipHits(Long_t Chip = 0);
		TBits *GetData() {return Data;}
		Long_t GetFrameNumber() {return FrameNumber;}
		Long_t GetHits();
		Long_t GetLineNumber() {return LineNumber;}
		void SetBitNumber(Long_t Number, Bool_t Val = kTRUE);
		void SetChannelDemuxed(Long_t Channel) {SetChannelState(Channel);}
		void SetChannelOff(Long_t Channel) {SetChannelState(Channel,kFALSE);}
		void SetChannelState(Long_t Channel, Bool_t Val = kTRUE);
		void SetFrameNumber(Long_t NewNumber) {FrameNumber = NewNumber;}
		void SetLineNumber(Long_t NewNumber) {LineNumber = NewNumber;}
		Bool_t TestBitNumber(Long_t Bit, Long_t Chip = 0);
	private:
		//data members
		static Long_t Counter;
		Long_t LineNumber, FrameNumber, Serial;
		TBits *Data, *WorkingChannels;
		static LookUpTables *LUT;
		//functions
		void Init();
		ClassDef(LineData, 2) //class title definition
};

#endif /* LINEDATA_H */
