#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class ChipPedestal+;
#pragma link C++ class ChipResponse+;
#pragma link C++ class DemuxedData+;
#pragma link C++ class DetectorResponse+;
#pragma link C++ class TestPatterns+;
#pragma link C++ class FOCALBuffer+;
#pragma link C++ class FOCALCluster+;
#pragma link C++ class FOCALFrame+;
#pragma link C++ class FOCALNewFrame+;
#pragma link C++ class LineData+;
#pragma link C++ class LookUpTables+;
#pragma link C++ class RawData+;
#pragma link C++ class SyncPoint+;
#pragma link C++ class SyncStream+;
#pragma link C++ class TestPattern+;
#pragma link C++ class TestPatterns+;
#pragma link C++ class PixelsArray+;
#pragma link C++ class LayerDensity+;
#pragma link C++ class StepDensity+;

#pragma link C++ class FOCALTracker+;
#pragma link C++ class FOCALTrack+;
#pragma link C++ class TrackCandidate+;
#pragma link C++ class TrackSignal+;
#pragma link C++ class FOCALHit+;
#pragma link C++ class FOCALNewHit+;

#pragma link C++ class FOCALTrigger+;
#pragma link C++ class TriggerHits+;
#pragma link C++ class TriggerStream+;

#pragma link C++ class VirtexHeader+;
#pragma link C++ class VirtexPedestal+;

#pragma link C++ class CreateDensities+;
#pragma link C++ class Analysis-;

#endif /* __CINT__ */
