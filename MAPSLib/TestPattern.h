#ifndef TESTPATTERN_H
#define TESTPATTERN_H

#include "LookUpTables.h"

class TestPattern : public TObject
{
	public:
		TestPattern();
		TestPattern(char *Input, Long_t Number = 0, Long_t PatternNumber = 0);
		TestPattern(const TestPattern& OtherObject);
		~TestPattern();
		TObject *Clone(const char *newname = "");
		Long_t GetChannel() {return ChannelNumber;}
		TBits *GetPattern() {return Data;}
		Long_t GetTestPatternNumber() {return TestPatternNumber;}
		void SetBitNumber(Long_t Number, Bool_t Val = kTRUE);
		Bool_t TestBitNumber(Long_t Number);
	private:
		//data members
		static Long_t Counter;
		Long_t ChannelNumber, TestPatternNumber, Serial;
		TBits *Data;
		static LookUpTables *LUT;
		//functions
		void Init(Long_t N = 0, Long_t P = 0);
		ClassDef(TestPattern, 1) //class title definition
};

#endif /* TESTPATTERNS_H */
