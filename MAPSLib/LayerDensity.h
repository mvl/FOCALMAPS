#ifndef LAYERDENSITY_H
#define LAYERDENSITY_H

#include "TClonesArray.h"

class LayerDensity : public TObject
{

public:
	LayerDensity();
	LayerDensity(const LayerDensity &OtherLayer);
	~LayerDensity();
	
	static const long totalsteps=126; 
	void Init();
	void SetLayernr(long vaule);
	long GetLayernr(){return Layernr;}
	
	//___________________________________________________	
	long Getnrhits(long step){ return nrhits[step]; }
	long Getworkingpixels(long step){ return workingpixels[step]; }
	long Getallpixels(long step){ return allpixels[step]; }
	long Getoverlappixels(long step){ return overlappixels[step]; }
	long Getoverlapwokp(long step){ return overlapwokp[step]; }
	double GetRadius(long step){ return Radius[step]; }
	double Getnoise(long step){ return noise[step]; }
	double GetOLnoise(long step){ return overlapnoise[step]; }
	void layerD( double radius,long value0, long value1, long value2, double value3,long value4, long value5, long value6, double value7, long step );
	void SetClusters( long *Cls, long step );
	long GetClusters( long step, long n ){ return Clusters[step][n]; }
	
	//________________________________________________________________________________________
	private:
	long Layernr, elements;
	
	double Radius[totalsteps*4],  noise[totalsteps*4],  overlapnoise[totalsteps*4];
	long nrhits[totalsteps*4], workingpixels[totalsteps*4], allpixels[totalsteps*4], overlappixels[totalsteps*4],	overlapwokp[totalsteps*4], nrhitsInOverlap[totalsteps*4];
	long Clusters[totalsteps][200];
	//______________________________________________________________________________________
	
	ClassDef(LayerDensity, 1) //class title definition
};
#endif		//LAYERDENSITY_H
