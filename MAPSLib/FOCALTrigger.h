#ifndef FOCALTRIGGER_H
#define FOCALTRIGGER_H

#include "LookUpTables.h"

class FOCALTrigger : public TObject
{
	public:
		//data members
		static const Long_t TriggerBits, FrameSyncBit, BeamPresenceBit;
		static const Long_t OverFlow2Start, OverFlow2Stop, OverFlowOffset, Spartan0SyncBit, Spartan1SyncBit;
		static const Long_t OverFlow1Start, OverFlow1Stop, VirtexCounterStart, VirtexCounterStop;
		static const Long_t TriggerCounterStart, TriggerCounterStop, TriggerBitStart, TriggerBitStop;
		static const Double_t TimePerCount;
		//functions
		FOCALTrigger();
		FOCALTrigger(const FOCALTrigger& OtherTrigger);
		~FOCALTrigger();
		Bool_t BitStatus(Long_t BitNumber);
		TObject *Clone(const char *newname = "");
		Bool_t IsDataTrigger() {return !(IsFrameSyncTrigger() || IsStartTrigger());}
		Bool_t IsFrameSyncTrigger() {return TriggerBit(FrameSyncBit);}
		Bool_t IsStartTrigger();
		Long_t LinesDifference(FOCALTrigger *OtherTrigger);
		Bool_t MatchTriggerBits(FOCALTrigger *OtherTrigger);
		Bool_t MatchTriggerCounter(FOCALTrigger *OtherTrigger);
		Long_t OverFlowStatus();
		void SetBit(Long_t BitNumber);
		Bool_t Spartan0Sync();
		Bool_t Spartan1Sync();
		Double_t Time();
		Bool_t TriggerBit(Long_t TriggerBitNumber);
		Long_t TriggerCounter();
		static Long_t TriggerCounterRange() {return 0x1<<(TriggerCounterStop-TriggerCounterStart);}
		void UnsetBit(Long_t BitNumber);
		Long_t VirtexCounter();
		static Long_t VirtexCounterRange() {return 0x1<<(VirtexCounterStop-VirtexCounterStart);}
		Long_t VirtexCounterDifference(FOCALTrigger *OtherTrigger);
	private:
		//data members
		static Long_t Counter;
		TBits *Data;
		static LookUpTables *LUT;
		Long_t Serial;
		//functions
		void Init();
		ClassDef(FOCALTrigger, 1) //class title definition
};


#endif /* FOCALTRIGGER_H */
