#ifndef CHIPPEDESTAL_H
#define CHIPPEDESTAL_H

#include "LookUpTables.h"
#include "DemuxedData.h"
#include "TriggerStream.h"

class TH1D;
class TH1I;
class TH2C;
class TH2D;
class TH3D;


class ChipPedestal : public TObject
{
	public:
		ChipPedestal();
		ChipPedestal(Long_t V, Long_t C, DemuxedData *DMD, TriggerStream *TS = NULL);
		ChipPedestal(const ChipPedestal& OtherObject);
		~ChipPedestal();
		TObject *Clone(const char *newname = "");
		Double_t GetAcceptThreshold() {return AcceptThreshold;}
		Double_t GetAverageNoise();
		Long_t GetChipNumber() {return Chip;}
		Long_t GetExcludedPixels();
		TH1I *GetNoiseSpectrum();
		Double_t GetNoiseThreshold();
		Double_t GetPixelNoise(Long_t Bit, Long_t Line);
		Double_t GetSigmaNoise();
		Long_t GetVirtexNumber() {return Virtex;}
		Bool_t IncludePixel(Long_t Bit, Long_t Line);
		Long_t LoadData(DemuxedData *DMD, TriggerStream *TS);
		TH1D *MakeDiscriminatorProjection();
		TH2C *MakeExcludeMap();
		TH2D *MakeHitMap();
		void PrintPedestalStatistics();
		void SetChipNumber(Long_t NewVal) {Chip = NewVal;}
		void SetVirtexNumber(Long_t NewVal) {Virtex = NewVal;}
		void SetAcceptThreshold(Double_t NewVal);
		TH2D *HitMap;
		Bool_t NoiseCalculated;
		TH1I *NoiseSpectrum;
	private:
		//data members
		static Double_t DefaultAcceptThreshold;
		static Long_t Counter;
		static LookUpTables *LUT;
		//static TH3D *NewMask;
		Long_t Virtex, Chip, ExcludedPixels, Serial;
		Double_t InternalNoiseThreshold, AcceptThreshold, AverageNoise, SigmaNoise;
                //TH3D* NewMask; 
		//functions
		void CalcNoise();
		void Init(Long_t V = -1, Long_t C = -1);
    //void LoadMask( );
		void ResetNoise();
		ClassDef(ChipPedestal, 1) //class title definition
};

#endif /* CHIPPEDESTAL_H */
