#ifndef FOCALFRAME_H
#define FOCALFRAME_H

#include "FOCALHit.h"
#include "TClonesArray.h"

class FOCALTrigger;
class VirtexPedestal;
class TH3C;
class TPolyMarker3D;

class FOCALFrame : public TObject
{
	public:
		//members
		Long_t Frame, Line, VirtexNumber, LinesToPreviousTrigger, LinesToNextTrigger, Run, Trigger, ShowerNumber;
		TBits *WorkingChannels, *LinesRead;
		TClonesArray *Hits, *Triggers;
		Bool_t DuplicatesRemoved;
		//functions
		FOCALFrame(Long_t Virtex = 0);
		FOCALFrame(const FOCALFrame& OtherFrame);
		~FOCALFrame();
		void AddHit(FOCALHit *NewHit);
		void AddHit(Long_t Chip, Long_t Line, Long_t Bit);
		Bool_t AllLinesPresent() {return (LinesPresent() == LUT->LinesPerFrame);}
		TObject *Clone(const char *newname = "");
		void CleanUp();
		void Draw(Option_t *option = "");
		FOCALHit *GetHit(Long_t Hit);
		Long_t GetHits(Long_t Chip = -1);
		Long_t GetEntries() {return Hits->GetEntriesFast();}
		FOCALTrigger *GetTrigger(Long_t Virtex);
		Long_t *HitsForLayer(Double_t X, Double_t Y, Double_t Z, Double_t Range) {return HitsForLayer(X,Y,FOCALHit::Layer(Z), Range);}
		Long_t *HitsForLayer(Double_t X, Double_t Y, Long_t Layer, Double_t Range);
		Long_t *NumberHitsForLayer(Double_t X=0, Double_t Y=0,Double_t Range=5);
		Bool_t IsComplete();
		Long_t LinesPresent() {return LinesRead->CountBits();}
		void LoadTrigger(FOCALTrigger *NewTrigger, Long_t Virtex = -1);
		TH3C *Make3D();
		TPolyMarker3D *MakeMarkers();
		TH3C *MakeWireFrame();
		void Merge(FOCALFrame *OtherFrame);
		Bool_t PixelWorking(Long_t Chip, Long_t Line, Long_t Bit);
		Bool_t PixelWorking(FOCALHit *Hit);
		Bool_t PixelWorking(Double_t X, Double_t Y, Double_t Z);
		Bool_t PixelWorking(Double_t X, Double_t Y, Long_t Layer);
		void RemoveHit(Long_t Hit);
		void SetChannelStatus(Long_t Channel, Bool_t Status = kTRUE);
		void SetLineStatus(Long_t LineNumber, Bool_t Status = kTRUE);
		Long_t TriggerCounter(Long_t Virtex = -1);
		Bool_t TriggerBit(Long_t Bit, Long_t Virtex = -1);
		Long_t VirtexCounter(Long_t Virtex = -1);
	private:
		//data members
		static LookUpTables *LUT;
		//functions
		Bool_t HitIsPresent(Long_t Hit);
		void Init();
		void RemoveDuplicates();
		ClassDef(FOCALFrame, 1) //class title definition
};

#endif //FOCALFRAME_H
