#ifndef TRACKSIGNAL_H
#define TRACKSIGNAL_H

#include "FOCALFrame.h"
#include "FOCALTrack.h"
#include "LookUpTables.h"
#include "TClonesArray.h"
#include "VirtexPedestal.h"

class TrackSignal : public TObject
{
	public:
		TrackSignal();
		TrackSignal(Long_t Number, FOCALTrack *NewTrack = NULL, FOCALFrame *NewFrame = NULL);
		~TrackSignal();
		Double_t InterceptX() {return IX;}
		Double_t InterceptY() {return IY;}
		Double_t SeedX() {return SX;}
		Double_t SeedY() {return SY;}
		Long_t GetHits() {return Hits->GetEntriesFast();}
		void LoadFrame(FOCALFrame *NewFrame);
		void LoadTrack(FOCALTrack *NewTrack);
		Long_t PixelsActive() {return NumberOfPixelsActive;}
		Long_t PixelsInArea() {return NumberOfPixelsInArea;}
		Long_t PixelsInLargestCluster() {return LargestCluster;}
		Long_t PixelsNotMasked() {return NumberOfPixelsUnMasked;}
		Long_t PixelsSampled() {return NumberOfPixelsSampled;}
		static void SetPedestals(VirtexPedestal **NewPointer);
	private:
		//members
		Bool_t TrackLoaded, FrameLoaded;
		Double_t IX, IY, SX, SY;
		Long_t ChipNumber, NumberOfPixelsSampled, NumberOfPixelsActive, NumberOfPixelsUnMasked, NumberOfPixelsInArea, LargestCluster, LPF, BPL;
		static LookUpTables *LUT;
		static VirtexPedestal **VP;
		TClonesArray *Hits;
		//functions
		void CleanUp();
		void Init();
		ClassDef(TrackSignal,1)
};

#endif //TRACKSIGNAL_H
