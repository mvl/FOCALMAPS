#ifndef VIRTEXHEADER_H
#define VIRTEXHEADER_H

#include "LookUpTables.h"
#include <time.h>

class VirtexHeader : public TObject
{
	public:
		//data members
		static const UInt_t HeaderSize = 32;  //header size in bytes
		TBits Bits;
		//functions
		VirtexHeader();
		VirtexHeader(char *Path);
		VirtexHeader(char *Data, Long_t Offset);
		VirtexHeader(const VirtexHeader& OtherObject);
		~VirtexHeader();
		Long_t BlockSize() {return MakeNumber(Blocksize_Bit_Start,Blocksize_Bit_Stop);}
		TObject *Clone(const char *newname = "");
		Long_t FirstFrame() {return MakeNumber(Frame_Bit_Start,Frame_Bit_Stop);}
		Bool_t IsLastBlock();
		Bool_t IsMuonData();
		Bool_t IsPedestalData();
		Bool_t IsTriggerData();
		void SetBit(Long_t Number, Bool_t Val = kTRUE) {return Bits.SetBitNumber(Number,Val);}
		Long_t SequenceNumber() {return MakeNumber(Sequence_Bit_Start, Sequence_Bit_Stop);}
		Bool_t TestBit(Long_t Number) {return Bits.TestBitNumber(Number);}
		time_t Time() {return (time_t) MakeNumber(Time_Bit_Start, Time_Bit_Stop);}
		Long_t TriggerCounter() {return MakeNumber(Counter_Bit_Start, Counter_Bit_Stop);}
		Long_t TriggerNumber() {return MakeNumber(Trigger_Bit_Start, Trigger_Bit_Stop);}
		Long_t Type() {return MakeNumber(Type_Bit_Start, Type_Bit_Stop);}
	private:
		//data members
		static LookUpTables *LUT;
		static Long_t GlobalSerial;
		//static const UInt_t Time_Bit_Start = 0, Time_Bit_Stop = 31, Sequence_Bit_Start = 32, Sequence_Bit_Stop = 63, Trigger_Bit_Start = 64, Trigger_Bit_Stop = 79, Counter_Bit_Start = 80, Counter_Bit_Stop = 95, Type_Bit_Start = 96, Type_Bit_Stop = 103, Blocksize_Bit_Start = 224, Blocksize_Bit_Stop = 255;
		static const Long_t Type_Bit_Start = 0, Type_Bit_Stop = 7, Time_Bit_Start = 8, Time_Bit_Stop = 39, Counter_Bit_Start = 40, Counter_Bit_Stop = 55, Frame_Bit_Start = 56, Frame_Bit_Stop = 71, Trigger_Bit_Start = 72, Trigger_Bit_Stop = 87, Sequence_Bit_Start = 88, Sequence_Bit_Stop = 119,  Blocksize_Bit_Start = 224, Blocksize_Bit_Stop = 255;
		Long_t Serial;
		//functions
		void Init();
		Long_t MakeNumber(Long_t FirstBit, Long_t LastBit);
		ClassDef(VirtexHeader, 1) //class title definition
};



#endif /* VIRTEXHEADER_H */
