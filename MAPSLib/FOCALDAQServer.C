//ROOT classes
#include "TInetAddress.h"
#include "TMethodCall.h"
#include "TROOT.h"
#include "TServerSocket.h"
#include "TSocket.h"
#include "TSystem.h"
#include "TThread.h"
//own classes
#include "LookUpTables.h"
#include "FOCALBuffer.h"
#include "VirtexHeader.h"

//global variables for access from threads
LookUpTables *LUT = LookUpTables::GetInstance();
Long_t NVirtex = 2, Run = 0, Trigger = 0;
TObjArray *ReceivedData = NULL;
TServerSocket **SocketsOpen = NULL;
TThread **Connections = NULL;
Bool_t Verbatim = kTRUE, Running = kTRUE;

void CleanUp();

void CloseSockets();

void Init();

void OpenDirectory();

Bool_t OpenSockets();

void *RootListen(void *In);

void WriteData();

void FOCALDAQServer(Bool_t Input = kTRUE)
{
	#ifdef __CINT__
	printf("This script can only be executed in compiled mode: .x \"filename+\"\n");
	return;
	#endif

	Init();

	Verbatim = Input;
	Int_t **SwitchArray = new Int_t*[NVirtex];
	Bool_t *VirtexFinished = new Bool_t[NVirtex];
	for(Long_t Virtex = 0; Virtex < NVirtex; Virtex++)
	{
		SwitchArray[Virtex] = new Int_t(Virtex);
		VirtexFinished[Virtex] = kTRUE;
	}  //end for Virtex

	while(Running)
	{
		if(!OpenSockets())
		{
			printf("Error occurred trying to open new connection, retrying in 1 second.\n");
			gSystem->Sleep(1000);
			continue;
		}else
		{
			if(Verbatim) printf("Making connection threads.\n");
		}  //end if !OpenSockets
		ReceivedData = new TObjArray(NVirtex);
		ReceivedData->SetOwner(kTRUE);
		for(Long_t Virtex = 0; Virtex < NVirtex; Virtex++)
		{
			TString ThreadName = "Connection thread for virtex #:";
			ThreadName += Virtex;
			Connections[Virtex] = new TThread(ThreadName, RootListen, (void*) SwitchArray[Virtex]);
			VirtexFinished[Virtex] = kFALSE;
			Connections[Virtex]->Run();
		}  //end for Virtex

		Bool_t Listening = kTRUE;
		if(Verbatim) printf("Waiting for new connections.\n");
		while(Listening)
		{
			gSystem->ProcessEvents();
			Listening = kFALSE;
			for(Long_t Virtex = 0; Virtex < NVirtex; Virtex++)
			{
				if(VirtexFinished[Virtex]) continue;
				if(Connections[Virtex]->GetState() == TThread::kCanceledState)
				{
					TObjArray *Arr = NULL;
					Connections[Virtex]->Join((void**)&Arr);
					if(Arr) ReceivedData->AddAt(Arr,Virtex);
					VirtexFinished[Virtex] = kTRUE;
					gSystem->Sleep(1);
				} //end if Connections[]->GetState()
				Listening = kTRUE;
			}  //end for Virtex
			//gSystem->Sleep(1);
		}  //end while Listening

		//save data	
		WriteData();
		CleanUp();
		//restart cycle
	}  //end while Running

	//garbage collection
	for(Long_t Virtex = 0; Virtex < NVirtex; Virtex++) delete SwitchArray[Virtex];
	delete [] VirtexFinished;
	delete [] SwitchArray;
	delete [] SocketsOpen;
	delete [] Connections;
}  //main program function

void CloseSockets()
{
	for(Long_t Virtex = 0; Virtex < NVirtex; Virtex++)
	{
		if(SocketsOpen[Virtex])
		{
			delete SocketsOpen[Virtex];
			SocketsOpen[Virtex] = NULL;
		}  //end if SocketsOpen[Virtex]
	}  //end for Virtex
}  //Closes all sockets presently in use by the server

void CleanUp()
{
	CloseSockets();
	for(Long_t Virtex = 0; Virtex < NVirtex; Virtex++)
	{
		if(Connections[Virtex])
		{
			delete Connections[Virtex];
			Connections[Virtex] = NULL;
		} //end if Connections[Virtex]
	}  //end for Virtex
	delete ReceivedData;
	ReceivedData = NULL;
}  //garbage collection

void Init()
{
	printf("Initializing server.\n");
	NVirtex = LUT->NVirtex;
	Connections = new TThread*[NVirtex];
	SocketsOpen = new TServerSocket*[NVirtex];
	for(Long_t Virtex = 0; Virtex < NVirtex; Virtex++)
	{
		Connections[Virtex] = NULL;
		SocketsOpen[Virtex] = NULL;
	}  //end for Virtex
	OpenDirectory();
}  //initialization function

void OpenDirectory()
{
	Bool_t SearchingForDir = kTRUE;
	while(SearchingForDir)
	{
		TString Path;
		Path.Form("%s%07ld/",LUT->FOCALDAQServer_DataPath,Run);
		if(Verbatim) printf("Trying %s\n", Path.Data());
		Int_t Status = gSystem->MakeDirectory(Path);
		if(Status == 0)
		{
			SearchingForDir = kFALSE;
			gSystem->cd(Path);
		}else
		{
			Run++;
		}  //end if Status
	}  //end while SearchingForDir

	printf("Saving data to path: %s\n", gSystem->pwd());
}  //search path for new free directory, use that as new saving path

Bool_t OpenSockets()
{
	Bool_t AllSuccesful = kTRUE;
	for(Long_t Virtex = 0; Virtex < NVirtex; Virtex++)
	{
		SocketsOpen[Virtex] = new TServerSocket(atoi(FOCALBuffer::Port(Virtex)),kTRUE);
		if(!SocketsOpen[Virtex]->IsValid())	AllSuccesful = kFALSE;
	}  //end for Virtex
	if(!AllSuccesful)
	{
		CloseSockets();
		return kFALSE;
	}  //end if
	for(Long_t Virtex = 0; Virtex < NVirtex; Virtex++) if(Verbatim) printf("Virtex #: %ld listening on port: %d\n",  Virtex, SocketsOpen[Virtex]->GetLocalPort());
	return kTRUE;
}  //attempts to open sockets for all Virtex boxes, returns kTRUE on succes

void *RootListen(void *In)
{
	Int_t Virtex = *((Int_t*) In);
	if(!SocketsOpen[Virtex])
	{
		printf("Virtex %d cannot open connection on non-existant socket.\n",Virtex);
		return NULL;
	}  //end if !SocketsOpen[Virtex]
	TSocket *S = SocketsOpen[Virtex]->Accept();
	SocketsOpen[Virtex]->Close();
	TObjArray *DataFromConnections = new TObjArray();
	DataFromConnections->SetOwner(kTRUE);
	while(1)
	{
		//receive the header
		char Buffer[VirtexHeader::HeaderSize] = {0x0};
		Long_t Received = S->RecvRaw(Buffer, VirtexHeader::HeaderSize);
		VirtexHeader *Header = new VirtexHeader(Buffer,0);
		if(Header->IsLastBlock())
		{
			delete Header;
			break;
		}  //end if PixelHeader->GetBlockSize()
		FOCALBuffer *CurrentConnection = new FOCALBuffer(Virtex);
		DataFromConnections->Add(Header);
		DataFromConnections->Add(CurrentConnection);
		CurrentConnection->SetSize(Header->BlockSize());
		if(Verbatim) printf("Virtex #: %d got message from: %s\nExpecting %ld bytes.\n", Virtex, (S->GetInetAddress()).GetHostAddress(), CurrentConnection->GetSize());

		//receive the main data package
		while(!CurrentConnection->Finished())
		{				
			Received = S->RecvRaw(CurrentConnection->CurrentPosition(), CurrentConnection->GetSize());
			CurrentConnection->IncreaseBytesReceived(Received);
			if(CurrentConnection->GetBytesReceived() == CurrentConnection->GetSize()) CurrentConnection->Finish();
		}  //end while !CurrentPixelConnection->Finished()
		if(Verbatim) printf("Virtex: %d, bytes received %ld/%ld.\n", Virtex, CurrentConnection->GetBytesReceived(), CurrentConnection->GetSize());
	}  //end while 1
	delete S;
	return DataFromConnections;
}  //connection handling thread

void WriteData()
{
	Bool_t MatchNumberOfEntries = kTRUE;
	for(Long_t Virtex = 1; Virtex < NVirtex; Virtex++)
	{
		TObjArray *CurrentData = (TObjArray*) ReceivedData->At(Virtex), *PrevData = (TObjArray*) ReceivedData->At(Virtex-1);
		if(!CurrentData || !PrevData || CurrentData->GetEntriesFast() != PrevData->GetEntriesFast())
		{
			printf("mismatch in the number of received objects between virtex %ld and %ld\n", Virtex, Virtex-1);
			MatchNumberOfEntries = kFALSE;
		}  //end if
	}  //end for Virtex
	if(!MatchNumberOfEntries) {printf("mismatched number of data entries, skipping data write step\n"); return;}

	Long_t RecdTriggers = (((TObjArray*) ReceivedData->At(0))->GetEntriesFast()/2)-1;
	for(Long_t Virtex = 0; Virtex < NVirtex; Virtex++)
	{
		TObjArray *CurrentData = (TObjArray*) ReceivedData->At(Virtex);
		if(!CurrentData)
		{
			printf("Error, no data received from virtex %ld.\n", Virtex);
			continue;
		}else
		{
			if(Verbatim) printf("Virtex #: %ld total received objects: %d\n", Virtex, CurrentData->GetEntriesFast());
		}  //end if CurrentData
		TObjArray *OpenFiles = new TObjArray();
		OpenFiles->SetOwner(kTRUE);

		//retrieve the trigger data and header, we will be writing multiple copies of this
		VirtexHeader *TriggerHeader = (VirtexHeader*) CurrentData->At(RecdTriggers*2);
		FOCALBuffer *TriggerData = (FOCALBuffer*) CurrentData->At(RecdTriggers*2+1);
		if(!TriggerHeader || !TriggerData)
		{
			printf("Non existant object.\n");
			continue;
		}else if(!TriggerHeader->IsTriggerData())
		{
			printf("Virtex #: %ld trigger data error!\nHeader indicates type %ld.\nHeader indicates size: %ld bytes.\n", Virtex, TriggerHeader->Type(), TriggerHeader->BlockSize());
			continue;
		}  //end if

		for(Long_t RecdTrigger = 0; RecdTrigger < RecdTriggers; RecdTrigger++)
		{
			VirtexHeader *PixelHeader = (VirtexHeader*) CurrentData->At(RecdTrigger*2);
			FOCALBuffer *PixelData = (FOCALBuffer*) CurrentData->At(RecdTrigger*2+1);
			TString PixelFile, TriggerFile, Path = gSystem->pwd();
			PixelFile.Form(LUT->PixelData_Filename_Format,Virtex,Trigger+RecdTrigger);
			TriggerFile.Form(LUT->TriggerData_Filename_Format,Virtex,Trigger+RecdTrigger);
			if(!PixelHeader->IsMuonData() && !PixelHeader->IsPedestalData())
			{
				printf("Virtex #: %ld pixel data error!\nHeader indicates type %ld.\nHeader indicates size: %ld bytes.\n", Virtex, PixelHeader->Type(), PixelHeader->BlockSize());
				continue;
			}  //end if

			//save the pixel data
			Path += "/";
			Path += PixelFile;
			TFile *SaveFile = new TFile(Path,"RECREATE");
			if(SaveFile->IsOpen())
			{
				if(Verbatim) printf("ROOT file opened succesfully.\n");
			}else
			{
				printf("Create error on file: %s\n", Path.Data());
				continue;
			}  //end if
			OpenFiles->Add(SaveFile);
			PixelData->Write();
			//save the pixel data header
			PixelFile.Form(LUT->PixelHeader_Filename_Format,Virtex,Trigger+RecdTrigger);
			Path = gSystem->pwd();
			Path += "/";
			Path += PixelFile;
			PixelHeader->SaveAs(Path,"q");
			

			//save the trigger data
			Path = gSystem->pwd();
			Path += "/";
			Path += TriggerFile;
			SaveFile = new TFile(Path,"RECREATE");
			if(SaveFile->IsOpen())
			{
				if(Verbatim) printf("ROOT file opened succesfully.\n");
			}else
			{
				printf("Create error on file: %s\n", Path.Data());
				continue;
			}  //end if
			OpenFiles->Add(SaveFile);
			TriggerData->Write();
			//save the trigger data header
			TriggerFile.Form(LUT->TriggerHeader_Filename_Format,Virtex,Trigger+RecdTrigger);
			Path = gSystem->pwd();
			Path += "/";
			Path += TriggerFile;
			TriggerHeader->SaveAs(Path,"q");
		}  //end for RecdTrigger

		for(Long_t File = 0; File < OpenFiles->GetEntriesFast(); File++)
		{
			TFile *OpenFile = (TFile*)OpenFiles->At(File);
			if(!OpenFile) continue;
			printf("Closing file.\n");
			OpenFile->Write();
			OpenFile->Close();
		}  //end for File
		delete OpenFiles;
		if(Verbatim) printf("Written virtex: %ld, trigger %ld to directory %s\n", Virtex, Trigger, gSystem->pwd());
	}  //end for Virtex
	printf("Run %ld, triggers %ld-%ld writing complete.\n", Run, Trigger, Trigger+RecdTriggers-1);
	Trigger += RecdTriggers;
}  //writes the received data to HDD
